<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesteamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salesteams', function (Blueprint $table) {
            $table->id();
            $table->string('fullname');
            $table->string('branch');
            $table->string('level');
            $table->string('position');
            $table->string('report');
            $table->char('targetdeals');
            $table->char('targetvisit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salesteams');
    }
}
