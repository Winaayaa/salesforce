<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reportsales', function (Blueprint $table) {
            $table->id();
            $table->string('sales');
            $table->char('total_visit');
            $table->char('total_deals');
            $table->char('prospecting');
            $table->char('negotiation');
            $table->char('won');
            $table->char('agreement');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reportsales');
    }
}
