<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadssTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leadss', function (Blueprint $table) {
            $table->id();
            $table->string('fullname');
            $table->char('phone');
            $table->string('email');
            $table->string('company');
            $table->char('com_phone');
            $table->string('com_address');
            $table->string('type');
            $table->string('channel');
            $table->string('customer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leadss');
    }
}
