<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class ChannelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            'Whatsapp',
            'Instagram',
            'Facebook',
            'Email',
            'Website',
            'Referral',
            'Other'
        ];

        foreach ($arr as $val) {
            DB::table('dat_master_channel')->insert([
                'id' => Str::uuid()->toString(),
                'name' => $val,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);
        }
    }
}
