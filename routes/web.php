<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Http\Controllers\ExampleController;
use App\Http\Controllers\SalesController;

header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Headers: *');

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// DASHBOARD
$router->get('/dashboard', function () use ($router) {
    return view('api.v1.dashboard.dashboard');
});

// MASTER (Sales Management)
//Sales
$router->get('/master/sales', 'SalesController@show');
$router->post('/master/sales/insert', 'SalesController@store');
$router->post('/master/sales/{id}', 'SalesController@update');
$router->post('/master/sales/delete/{id}', 'SalesController@delete');
$router->get('/master/sales/find', 'SalesController@search');
// Rewards
$router->get('/master/reward', 'RewardController@show');
$router->post('/master/reward/insert', 'RewardController@store');
$router->post('/master/reward/{id}', 'RewardController@update');
$router->post('/master/reward/delete/{id}', 'RewardController@delete');
$router->get('/master/reward/find', 'RewardController@search');

// LEADS (Customer Management)
//Leads
$router->get('/leads', 'LeadsController@show');
$router->post('/leads/insert', 'LeadsController@store');
$router->post('/leads/{id}', 'LeadsController@update');
$router->post('/leads/delete/{id}', 'LeadsController@delete');
$router->get('/leads/find', 'LeadsController@search');
$router->get('/leads/filter', 'LeadsController@filter');
//Customer
$router->get('/leads/customer', 'CustomerController@show');
$router->post('/leads/customer/insert', 'CustomerController@store');
$router->post('/leads/customer/{id}', 'CustomerController@update');
$router->post('/leads/customer/delete/{id}', 'CustomerController@delete');
$router->get('/leads/customer/find', 'CustomerController@search');
$router->get('/leads/customer/filter', 'CustomerController@filter');

// SCHEDULE (Visit Management)
//Schedules
$router->get('/schedule', 'ScheduleController@show');
$router->post('/schedule/insert', 'ScheduleController@store');
$router->post('/schedule/{id}', 'ScheduleController@update');
$router->post('/schedule/delete/{id}', 'ScheduleController@delete');
//Requests
$router->get('/schedule/requests', 'RequestsController@show');
$router->post('/schedule/requests/insert', 'RequestsController@store');
$router->post('/schedule/requests/{id}', 'RequestsController@update');
$router->post('/schedule/requests/delete/{id}', 'RequestsController@delete');
$router->get('/schedule/requests/find', 'RequestsController@search');

// REPORT
//Visit
$router->get('/report', function () use ($router) {
    return view('api.v1.report.visit');
});
//Sales
$router->get('/report/sales', function () use ($router) {
    return view('api.v1.report.sales');
});
