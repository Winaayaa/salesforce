@extends('app')
@section('customer', 'link-primary')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <nav aria-label="breadcrumb" style="--bs-breadcrumb-divider: '>';">
        <h2>Customer Management</h2>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#" style="text-decoration: none; color: black">Customer Management</a></li>
          <li class="breadcrumb-item active" aria-current="page" style="color:rgb(139, 178, 225)">Customer</li>
        </ol>
      </nav>
        {{-- <h6>Customer</h6> --}}
        <div class="btn-toolbar mb-2 mb-md-0">
            {{-- <button type="button" class="btn btn-md btn-success"><strong>+ Tambah</strong></button> --}}
            <button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#addCustomer" style="background-color: rgb(139, 178, 225)">
                <strong>+ Tambah</strong>
            </button>
            
            <!-- Modal Add Customer Management -->
            <div class="modal fade" id="addCustomer" tabindex="-1" aria-labelledby="addSales" aria-hidden="true">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="background-color:rgb(139, 178, 225)">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Add Customer</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      <form method="post" action="/leads/customer/insert">
                        <div class="mb-3">
                          <label for="id" class="form-label">ID Customer</label>
                          <input type="text" class="form-control" id="id" name="id">
                        </div>
                        <div class="mb-3">
                          <label for="fullname" class="form-label">Fullname</label>
                          <input type="text" class="form-control" id="fullname" name="fullname">
                        </div>
                        <div class="mb-3">
                          <label for="phone" class="form-label">Phone Number</label>
                          <input type="text" class="form-control" id="phone" name="phone">
                        </div>
                        <div class="mb-3">
                          <label for="company" class="form-label">Company Name</label>
                          <input type="text" class="form-control" id="company" name="company">
                        </div>
                        <div class="mb-3">
                          <label for="com_phone" class="form-label">Company Phone</label>
                          <input type="text" class="form-control" id="com_phone" name="com_phone">
                        </div>
                        <div class="mb-3">
                          <label for="type" class="form-label">Leads Type</label>
                          <select class="form-select" aria-label=".form-select-sm example" id="type" name="type">
                            <option selected>Choose Leads Type</option>
                            <option value="Prospecting">Prospecting</option>
                            <option value="Demo or Meeting">Demo or Meeting</option>
                            <option value="Sign Agreement">Sign Agreement</option>
                            <option value="Negotiation">Negotiation</option>
                          </select>
                        </div>
                        <div class="mb-3">
                          <label for="assign_to" class="form-label">Assign to</label>
                          <input type="text" class="form-control" id="assign_to" name="assign_to">
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn rounded-pill btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                          <button type="submit" class="btn btn-primary rounded-pill">Save</button>
                          </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              {{-- End Modal --}}
          </div>
        </div>

      <ul class="nav nav-tabs">
        <li class="nav-item">
          <a class="nav-link active" style="color: rgb(139, 178, 225)" aria-current="page" href="/leads" 
          role="button" aria-expanded="false" aria-controls="leads"><strong>Leads</strong></a>
        </li>
        <li class="nav-item">
            <a class="nav-link link-light" style="background-color: rgb(139, 178, 225)" aria-current="page" href="/leads/customer" 
            role="button" aria-expanded="false" aria-controls="customer"><strong>Customer</strong></a>
        </li>
      </ul>

      <div class="container-fluid mb-2 mt-2">
        <div class="mb-2">
          <form action="/leads/customer/filter" method="get">
            Filter
            <select class="form-select d-inline-flex p-2" name="type" aria-label="Default select example" style="width: 150px">
              <option selected>All Type</option>
              <option value="Prospecting">Prospecting</option>
              <option value="Demo or Meeting">Demo or Meeting</option>
              <option value="Sign Agreement">Sign Agreement</option>
              <option value="Negotiation">Negotiation</option>
            </select>
            <select class="form-select d-inline-flex p-2" name="assign_to" aria-label="Default select example" style="width: 150px">
              <option selected>All Assign To</option>
              @forelse ($data as $item)
                <option value="{{ $item->id }}">{{ $item->assign_to }}</option>
              @empty
                <p>Data is empty</p>
              @endforelse
            </select>
            <button class="btn btn-sm" type="submit">
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-filter" viewBox="0 0 16 16">
                <path d="M6 10.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 0 1h-3a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"/>
              </svg>
            </button>
          </form>
          {{-- <button type="button" class="btn btn-outline-success" style="width: 250px; position: absolute; left: 80%">
            <strong>
              Move to Customer
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-90deg-right" viewBox="0 0 16 16">
              <path fill-rule="evenodd" d="M14.854 4.854a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708.708L13.293 4H3.5A2.5 2.5 0 0 0 1 6.5v8a.5.5 0 0 0 1 0v-8A1.5 1.5 0 0 1 3.5 5h9.793l-3.147 3.146a.5.5 0 0 0 .708.708l4-4z"/>
            </svg>
            </strong>
          </button> --}}
        </div>

        Show <select class="form-select d-inline-flex p-2" aria-label="Default select example" style="width: 70px">
          <option selected>{{ $data->count() }}</option>
          <option value="1">5</option>
          <option value="2">10</option>
          <option value="3">25</option>
        </select>
        entries
        <form class="form d-inline-flex" action="/leads/customer/find" method="get"
          style="width: 250px; position: absolute; left: 80%">
            <input type="text" name="search" class="form-control" placeholder="Search..." value="{{ request('search') }}">
            {{-- <input type="submit" value="Search"> --}}
            <button class="btn btn-sm" type="submit" style="background-color:rgb(139, 178, 225)">
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
              </svg>
            </button>
          </form>
      </div>

      <table class="table">
        <thead style="background-color:rgb(139, 178, 225)">
          <tr>
            <th scope="col">No</th>
            <th scope="col">Full Name</th>
            <th scope="col">Phone Number</th>
            <th scope="col">Company Name</th>
            <th scope="col">Company Phone</th>
            <th scope="col">Leads Type</th>
            <th scope="col">Assign to</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        
        @forelse ($data as $customer)
        <tbody>
          <tr>
            <th scope="row">{{ $customer->id }}</th>
            <td>{{ $customer->fullname }}</td>
            <td>{{ $customer->phone }}</td>
            <td>{{ $customer->company }}</td>
            <td>{{ $customer->com_phone }}</td>
            <td>{{ $customer->type }}</td>
            <td>{{ $customer->assign_to }}</td>
            <td>
              <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#editCustomer{{ $customer->id }}">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pen" viewBox="0 0 16 16">
                    <path d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001zm-.644.766a.5.5 0 0 0-.707 0L1.95 11.756l-.764 3.057 3.057-.764L14.44 3.854a.5.5 0 0 0 0-.708l-1.585-1.585z"/>
                  </svg>
              </button>

                <button type="button" class="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#deleteCustomer{{ $customer->id }}">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>
                    <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>
                  </svg>
              </button>
              
              <!-- Modal Edit Customer Management -->
              <div class="modal fade" id="editCustomer{{ $customer->id }}" tabindex="-1" aria-labelledby="editSales" aria-hidden="true">
                  <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header" style="background-color:rgb(139, 178, 225)">
                      <h1 class="modal-title fs-5" id="exampleModalLabel">Edit Customer</h1>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <div class="modal-body">
                        <form method="post" action="/leads/customer/{{ $customer->id }}">
                          <div class="mb-3">
                            <label for="fullname" class="form-label">Fullname</label>
                            <input type="text" class="form-control" id="fullname" name="fullname" value="{{ $customer->fullname }}">
                          </div>
                          <div class="mb-3">
                            <label for="phone" class="form-label">Phone Number</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{ $customer->phone }}">
                          </div>
                          <div class="mb-3">
                            <label for="company" class="form-label">Company Name</label>
                            <input type="text" class="form-control" id="company" name="company" value="{{ $customer->company }}">
                          </div>
                          <div class="mb-3">
                            <label for="com_phone" class="form-label">Company Phone</label>
                            <input type="text" class="form-control" id="com_phone" name="com_phone" value="{{ $customer->com_phone }}">
                          </div>
                          <div class="mb-3">
                            <label for="type" class="form-label">Leads Type</label>
                            <input type="text" class="form-control" id="type" name="type" value="{{ $customer->type }}">
                          </div>
                          <div class="mb-3">
                            <label for="assign_to" class="form-label">Assign to</label>
                            <input type="text" class="form-control" id="assign_to" name="assign_to" value="{{ $customer->assign_to }}">
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn rounded-pill btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary rounded-pill">Update</button>
                            </div>
                        </form>
                      </div>
                    </div>
                  </div>
              </div>

              <!-- Modal Delete Customer Management -->
              <div class="modal fade" id="deleteCustomer{{ $customer->id }}" tabindex="-1" aria-labelledby="deleteSales" aria-hidden="true">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Delete Customer</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      <form method="post" action="/leads/delete/{{ $customer->id }}">
                        <h6>Apakah Anda yakin ingin menghapus data {{ $customer->fullname }} ?</h6>
                        <div class="modal-footer">
                          <button type="button" class="btn rounded-pill btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                          <button type="submit" class="btn btn-danger rounded-pill">Delete</button>
                          </div>
                      </form>
                    </div>
                  </div>
                </div>
            </div>
            </td>
          </tr>
        </tbody>
        @empty
            <p>Data tidak ditemukan.</p>
        @endforelse
      </table>

      <div class="container-fluid mb-2 mt-2 d-inline-flex p-2">
        Showing 1 to {{ $data->count() }} of {{ $data->count() }} entries

        <nav aria-label="...">
          <ul class="pagination" style="position: absolute; left: 80%;">
            <li class="page-item disabled">
              <a class="page-link">Previous</a>
            </li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item" aria-current="page">
              <a class="page-link" href="#">2</a>
            </li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
              <a class="page-link" href="#">Next</a>
            </li>
          </ul>
        </nav>
      </div>

    <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>
@endsection