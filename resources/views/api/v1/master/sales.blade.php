@extends('app')
@section('masterSales', 'link-primary')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <nav aria-label="breadcrumb" style="--bs-breadcrumb-divider: '>';">
        <h2>Sales Management</h2>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#" style="text-decoration: none; color: black">Sales Management</a></li>
          <li class="breadcrumb-item active" aria-current="page" style="color:rgb(139, 178, 225)">Sales Team</li>
        </ol>
      </nav>
        <div class="btn-toolbar mb-2 mb-md-0">
            {{-- <button type="button" class="btn btn-md btn-success"><strong>+ Tambah</strong></button> --}}
            <!-- Button trigger modal -->
        <button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#addSales" style="background-color:rgb(139, 178, 225)">
            <strong>+ Tambah</strong>
        </button>
        
        <!-- Modal Add Sales Team -->
        <div class="modal fade" id="addSales" tabindex="-1" aria-labelledby="addSales" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(139, 178, 225)">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Add Sales Team</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                  <form method="post" action="/master/sales/insert">
                    <div class="mb-3">
                      <label for="id" class="form-label">ID Employee</label>
                      <input type="text" class="form-control" id="id" name="id">
                    </div>
                    <div class="mb-3">
                      <label for="fullname" class="form-label">Employee</label>
                      <input type="text" class="form-control" id="fullname" name="fullname">
                    </div>
                    <div class="form inline">
                      <div class="form-group">
                        <label for="branch" class="form-label">Branch</label>
                        <input type="text" class="form-control" id="branch" name="branch">
                      </div>
                      <div class="form-group">
                        <label for="level" class="form-label">Level</label>
                        <input type="text" class="form-control" id="level" name="level">
                      </div>
                    </div>
                    <div class="mb-3">
                      <label for="position" class="form-label">Position</label>
                      <input type="text" class="form-control" id="position" name="position">
                    </div>
                    <div class="mb-3">
                      <label for="report" class="form-label">Report To</label>
                      <input type="text" class="form-control" id="report" name="report">
                    </div>
                    <div class="mb-3">
                      <label for="targetdeals" class="form-label">Target Deals</label>
                      <input type="text" class="form-control" id="targetdeals" name="targetdeals">
                    </div>
                    <div class="mb-3">
                      <label for="targetvisit" class="form-label">Target Visit</label>
                      <input type="text" class="form-control" id="targetvsiit" name="targetvisit">
                    </div>
                    <div class="mb-3 form-check">
                      <input type="checkbox" class="form-check-input" id="exampleCheck1">
                      <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn rounded-pill btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                      <button type="submit" class="btn btn-primary rounded-pill">Save</button>
                      </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>

    <ul class="nav nav-tabs">
        <li class="nav-item">
          <a class="nav-link link-light" style="background-color:rgb(139, 178, 225)" aria-current="page" href="/master/sales" 
          role="button" aria-expanded="false" aria-controls="leads"><strong>Sales Team</strong></a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" style="color: rgb(139, 178, 225)" aria-current="page" href="/master/reward" 
            role="button" aria-expanded="false" aria-controls="activity"><strong>Rewards</strong></a>
        </li>
      </ul>

      <div class="container-fluid mb-2 mt-2">
        <span>Show</span>
        <select class="form-select d-inline-flex p-2" aria-label="Default select example" style="width: 70px">
          <option selected>{{ $data->count() }}</option>
          <option value="1">5</option>
          <option value="2">10</option>
          <option value="3">25</option>
        </select>
        entries
          <form class="form d-inline-flex" action="/master/sales/find" method="get"
          style="width: 250px; position: absolute; left: 80%">
            <input type="text" name="search" class="form-control" placeholder="Search..." value="{{ request('search') }}">
            {{-- <input type="submit" value="Search"> --}}
            <button class="btn btn-sm" type="submit" style="background-color:rgb(139, 178, 225)">
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
              </svg>
            </button>
          </form>
      </div>

      <table class="table">
        <thead style="background-color:rgb(139, 178, 225)">
          <tr>
            <th scope="col">No.</th>
            <th scope="col">User Number</th>
            <th scope="col">Full Name</th>
            <th scope="col">Branch</th>
            <th scope="col">Position</th>
            <th scope="col">Level</th>
            <th scope="col">Target Deals</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        
        @forelse ($data as $sales)
        <tbody>
          <tr>
            <th scope="row">{{ $sales->id }}</th>
            <td>ID000{{ $sales->id }}</td>
            <td>{{ $sales->fullname }}</td>
            <td>{{ $sales->branch }}</td>
            <td>{{ $sales->position }}</td>
            <td>{{ $sales->level }}</td>
            <td>Rp{{ $sales->targetdeals }}</td>
            <td>
              <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#editSales{{ $sales->id }}">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pen" viewBox="0 0 16 16">
                  <path d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001zm-.644.766a.5.5 0 0 0-.707 0L1.95 11.756l-.764 3.057 3.057-.764L14.44 3.854a.5.5 0 0 0 0-.708l-1.585-1.585z"/>
                </svg>
            </button>

              <button type="button" class="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#deleteSales{{ $sales->id }}">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>
                  <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>
                </svg>
            </button>
            
            <!-- Modal Edit Sales Team -->
            <div class="modal fade" id="editSales{{ $sales->id }}" tabindex="-1" aria-labelledby="editSales" aria-hidden="true">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="background-color:rgb(139, 178, 225)">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Edit Sales Team</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      <form method="post" action="/master/sales/{{ $sales->id }}">
                        <div class="mb-3">
                          <label for="id" class="form-label">ID Employee</label>
                          <input type="text" class="form-control" id="id" name="id" value="ID000{{ $sales->id }}" disabled>
                        </div>
                        <div class="mb-3">
                          <label for="fullname" class="form-label">Employee</label>
                          <input type="text" class="form-control" id="fullname" name="fullname" value="{{ $sales->fullname }}">
                        </div>
                        <div class="mb-3">
                          <label for="branch" class="form-label">Branch</label>
                          <input type="text" class="form-control" id="branch" name="branch" value="{{ $sales->branch }}">
                        </div>
                        <div class="mb-3">
                          <label for="level" class="form-label">Level</label>
                          <input type="text" class="form-control" id="level" name="level" value="{{ $sales->level }}">
                        </div>
                        <div class="mb-3">
                          <label for="position" class="form-label">Position</label>
                          <input type="text" class="form-control" id="position" name="position" value="{{ $sales->position }}">
                        </div>
                        <div class="mb-3">
                          <label for="report" class="form-label">Report To</label>
                          <input type="text" class="form-control" id="report" name="report" value="{{ $sales->report }}">
                        </div>
                        <div class="mb-3">
                          <label for="targetdeals" class="form-label">Target Deals</label>
                          <input type="text" class="form-control" id="targetdeals" name="targetdeals" value="{{ $sales->targetdeals }}">
                        </div>
                        <div class="mb-3">
                          <label for="targetvisit" class="form-label">Target Visit</label>
                          <input type="text" class="form-control" id="targetvsiit" name="targetvisit" value="{{ $sales->targetvisit }}">
                        </div>
                        <div class="mb-3 form-check">
                          <input type="checkbox" class="form-check-input" id="exampleCheck1">
                          <label class="form-check-label" for="exampleCheck1">Check me out</label>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn rounded-pill btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                          <button type="submit" class="btn btn-primary rounded-pill">Update</button>
                          </div>
                      </form>
                    </div>
                  </div>
                </div>
            </div>

            <!-- Modal Delete Sales Team -->
            <div class="modal fade" id="deleteSales{{ $sales->id }}" tabindex="-1" aria-labelledby="deleteSales" aria-hidden="true">
              <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header bg-danger">
                  <h1 class="modal-title fs-5" id="exampleModalLabel">Delete Sales Team</h1>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    <form method="post" action="/master/sales/delete/{{ $sales->id }}">
                      <h6>Apakah Anda yakin ingin menghapus {{ $sales->fullname }} ?</h6>
                      <div class="modal-footer">
                        <button type="button" class="btn rounded-pill btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger rounded-pill">Delete</button>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
          </div>
            </td>
          </tr>
        </tbody>
        @empty
            <p>Tidak ditemukan data...</p>
        @endforelse
      </table>

      <div class="container-fluid mb-2 mt-2 d-inline-flex p-2">
        Showing 1 to {{ $data->count() }} of {{ $data->count() }} entries

        <nav aria-label="...">
          <ul class="pagination" style="position: absolute; left: 80%;">
            <li class="page-item disabled">
              <a class="page-link">Previous</a>
            </li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item" aria-current="page">
              <a class="page-link" href="#">2</a>
            </li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
              <a class="page-link" href="#">Next</a>
            </li>
          </ul>
        </nav>
      </div>


    <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>
@endsection

