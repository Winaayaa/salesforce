@extends('app')
@section('masterSales', 'link-primary')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <nav aria-label="breadcrumb" style="--bs-breadcrumb-divider: '>';">
        <h2>Sales Management</h2>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#" style="text-decoration: none; color: black">Sales Management</a></li>
          <li class="breadcrumb-item active" aria-current="page" style="color:rgb(139, 178, 225)">Rewards</li>
        </ol>
      </nav>
        <div class="btn-toolbar mb-2 mb-md-0">
            {{-- <button type="button" class="btn btn-md btn-success"><strong>+ Tambah</strong></button> --}}
            <button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#addSales" style="background-color: rgb(139, 178, 225)">
              <strong>+ Tambah</strong>
          </button>
          <div class="modal fade" id="addSales" tabindex="-1" aria-labelledby="addSales" aria-hidden="true">
            <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgb(139, 178, 225)">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Add Sales Team</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                  <form method="post" action="/master/reward/insert">
                    <div class="mb-3">
                      <label for="id" class="form-label">ID Reward</label>
                      <input type="text" class="form-control" id="id" name="id">
                    </div>
                    <div class="mb-3">
                      <label for="name" class="form-label">Rewards Name</label>
                      <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="row gx-5">
                      <div class="col mb-3">
                        <label for="from" class="form-label">From Date</label>
                        <input type="date" class="form-control" id="from" name="from">
                      </div>
                      <div class="col mb-3">
                        <label for="to" class="form-label">To Date</label>
                        <input type="date" class="form-control d-inline-flex" id="to" name="to">
                      </div>
                    </div>
                    {{-- <div class="mb-3">
                      <label for="from" class="form-label">From Date</label>
                      <input type="date" class="form-control" id="from" name="from">
                    </div>
                    <div class="mb-3">
                      <label for="to" class="form-label">To Date</label>
                      <input type="date" class="form-control d-inline-flex" id="to" name="to">
                    </div> --}}
                    <div class="row gx-5">
                      <div class="col mb-3">
                        <label for="value" class="form-label">Value(point)</label>
                        <input type="text" class="form-control" id="value" name="value">
                      </div>
                      <div class="col mb-3">
                        <label for="parameter" class="form-label">Parameter</label>
                      <input type="text" class="form-control" id="parameter" name="parameter">
                      </div>
                    </div>
                    {{-- <div class="mb-3">
                      <label for="value" class="form-label">Value(point)</label>
                      <input type="text" class="form-control" id="value" name="value">
                    </div>
                    <div class="mb-3">
                      <label for="parameter" class="form-label">Parameter</label>
                      <input type="text" class="form-control" id="parameter" name="parameter">
                    </div> --}}
                    <h5>Set Goals</h5>
                    <div class="mb-3">
                      Reward Type
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="type" id="type" value="Amount">
                        <label class="form-check-label" for="type">
                          Amount
                        </label>
                      </div>   
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="type" id="type" value="Other">
                        <label class="form-check-label" for="type">
                          Other
                        </label>
                      </div>                      
                    </div>
                    <div class="mb-3">
                      <label for="gift" class="form-label">Gift</label>
                      <input type="text" class="form-control" id="gift" name="gift">
                    </div>
                    <div class="row gx-5">
                      <div class="col mb-3">
                        <label for="target" class="form-label">Target</label>
                        <input type="text" class="form-control" id="target" name="target">
                      </div>
                      <div class="col mb-3">
                        <label for="equals" class="form-label">Equals To (Point)</label>
                        <input type="text" class="form-control" id="equals" disabled>
                      </div>
                    </div>
                    {{-- <div class="mb-3">
                      <label for="target" class="form-label">Target</label>
                      <input type="text" class="form-control" id="target" name="target">
                    </div> --}}
                    Set Sales Team
                    <div class="container-fluid mb-2 mt-2">
                      <div class="row gx-5">
                        <div class="col mb-3">
                          <span>Show</span>
                          <select class="form-select d-inline-flex p-2" aria-label="Default select example" style="width: 70px">
                            <option selected>{{ $data->count() }}</option>
                            <option value="1">5</option>
                            <option value="2">10</option>
                            <option value="3">15</option>
                          </select>
                          entries
                        </div>
                        <div class="col-3">
                          <form class="form d-inline-flex" action="/master/reward/find" method="get"
                            style="width: 50px; position: absolute;">
                              <input type="text" name="search" class="form-control" placeholder="Search..." value="{{ request('search') }}">
                              {{-- <input type="submit" value="Search"> --}}
                              <button class="btn btn-sm" type="submit" style="background-color:rgb(139, 178, 225)">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                  <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                                </svg>
                              </button>
                          </form>
                        </div>
                      </div>
                    </div>

                    <table class="table">
                      <thead style="background-color:rgb(139, 178, 225)">
                        <tr>
                          <th scope="col">No</th>
                          <th scope="col">Nama Rewards</th>
                          <th scope="col">From Date</th>
                        </tr>
                      </thead>
                      {{-- @forelse ($data as $rewards) --}}
                      <tbody>
                        <tr>
                          <th scope="row">kotak</th>
                          <td>warna</td>
                          <td>biru</td>
                        </tbody>
                    </table>

                    <div class="modal-footer">
                      <button type="button" class="btn rounded-pill btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                      <button type="submit" class="btn btn-primary rounded-pill">Save</button>
                      </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {{-- <div class="container-fluid mb-2">
        Show
        <input type="text" style="width: 50px" value="{{ $data->count() }}">
        entries
      </div> --}}

    <ul class="nav nav-tabs">
        <li class="nav-item">
          <a class="nav-link active" style="color: rgb(139, 178, 225)" aria-current="page" href="/master/sales" 
          role="button" aria-expanded="false" aria-controls="leads"><strong>Sales Team</strong></a>
        </li>
        <li class="nav-item">
            <a class="nav-link link-light" style="background-color: rgb(139, 178, 225)" aria-current="page" href="/master/reward" 
            role="button" aria-expanded="false" aria-controls="customer"><strong>Rewards</strong></a>
        </li>
      </ul>

      <div class="container-fluid mb-2 mt-2">
        <span>Show</span>
        <select class="form-select d-inline-flex p-2" aria-label="Default select example" style="width: 70px">
          <option selected>{{ $data->count() }}</option>
          <option value="1">5</option>
          <option value="2">10</option>
          <option value="3">15</option>
        </select>
        entries
        <form class="form d-inline-flex" action="/master/reward/find" method="get"
          style="width: 250px; position: absolute; left: 80%">
            <input type="text" name="search" class="form-control" placeholder="Search..." value="{{ request('search') }}">
            {{-- <input type="submit" value="Search"> --}}
            <button class="btn btn-sm" type="submit" style="background-color:rgb(139, 178, 225)">
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
              </svg>
            </button>
        </form>
      </div>

      <table class="table">
        <thead style="background-color:rgb(139, 178, 225)">
          <tr>
            <th scope="col">No</th>
            <th scope="col">Nama Rewards</th>
            <th scope="col">From Date</th>
            <th scope="col">To Date</th>
            <th scope="col">Value</th>
            <th scope="col">Parameter</th>
            <th scope="col">Rewards Type</th>
            <th scope="col">Target</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        
        @forelse ($data as $rewards)
        <tbody>
          <tr>
            <th scope="row">{{ $rewards->id }}</th>
            <td>{{ $rewards->name }}</td>
            <td>{{ $rewards->from }}</td>
            <td>{{ $rewards->to }}</td>
            <td>{{ $rewards->value }} Point</td>
            <td>Rp{{ $rewards->parameter }}</td>
            <td>{{ $rewards->type }}</td>
            <td>Rp{{ $rewards->target }}</td>
            <td>
              <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#editReward{{ $rewards->id }}">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pen" viewBox="0 0 16 16">
                  <path d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001zm-.644.766a.5.5 0 0 0-.707 0L1.95 11.756l-.764 3.057 3.057-.764L14.44 3.854a.5.5 0 0 0 0-.708l-1.585-1.585z"/>
                </svg>
            </button>
      
              <button type="button" class="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#deleteReward{{ $rewards->id }}">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>
                  <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>
                </svg>
            </button>
            
            <!-- Modal Edit Sales Reward -->
            <div class="modal fade" id="editReward{{ $rewards->id }}" tabindex="-1" aria-labelledby="editReward" aria-hidden="true">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="background-color:rgb(139, 178, 225)">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Edit Sales Reward</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      <form method="post" action="/master/reward/{{ $rewards->id }}">
                        <input type="text" class="form-control" id="id" name="id" value="{{ $rewards->id }}" disabled>
                        <div class="mb-3">
                          <label for="name" class="form-label">Rewards Name</label>
                          <input type="text" class="form-control" id="name" name="name" value="{{ $rewards->name }}">
                        </div>
                        <div class="mb-3">
                          <label for="from" class="form-label">From Date</label>
                          <input type="date" class="form-control" id="from" name="from" value="{{ $rewards->from }}">
                        </div>
                        <div class="mb-3">
                          <label for="to" class="form-label">To Date</label>
                          <input type="date" class="form-control" id="to" name="to" value="{{ $rewards->to }}">
                        </div>
                        <div class="mb-3">
                          <label for="value" class="form-label">Value(point)</label>
                          <input type="text" class="form-control" id="value" name="value" value="{{ $rewards->value }}">
                        </div>
                        <div class="mb-3">
                          <label for="parameter" class="form-label">Parameter</label>
                          <input type="text" class="form-control" id="parameter" name="parameter" value="{{ $rewards->parameter }}">
                        </div>
                        <h4>Set Goals</h4>
                        <div class="mb-3">
                          Reward Type
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="type" id="type" value="Amount">
                            <label class="form-check-label" for="type">
                              Amount
                            </label>
                          </div>   
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="type" id="type" value="Other">
                            <label class="form-check-label" for="type">
                              Other
                            </label>
                          </div>                      
                        </div>
                        <div class="mb-3">
                          <label for="gift" class="form-label">Gift</label>
                          <input type="text" class="form-control" id="gift" name="gift" value="{{ $rewards->gift }}">
                        </div>
                        <div class="mb-3">
                          <label for="target" class="form-label">Target</label>
                          <input type="text" class="form-control" id="target" name="target" value="{{ $rewards->target }}">
                        </div>
                        {{-- <div class="mb-3">
                            <label for="equals" class="form-label">Equals to (Point)</label>
                            <input type="type" class="form-control" id="equals">
                          </div>
                          <div class="mb-3">
                            Set Sales Team
                            <table class="table">
                              <thead>
                                <tr>
                                  <th scope="col">No</th>
                                  <th scope="col">User Number</th>
                                  <th scope="col">Full Name</th>
                                  <th scope="col"></th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <th scope="row">
                                    <div class="form-check">
                                      <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                      <label class="form-check-label" for="flexCheckDefault">
                                        
                                      </label>
                                    </div>
                                  </th>
                                  <td>00030</td>
                                  <td>Bima Pratama</td>
                                  <td></td>
                                </tr>
                              </tbody>
                            </table> --}}
                        <div class="modal-footer">
                          <button type="button" class="btn rounded-pill btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                          <button type="submit" class="btn btn-primary rounded-pill">Save</button>
                          </div>
                      </form>
                    </div>
                  </div>
                </div>
            </div>
      
            <!-- Modal Delete Sales Reward -->
            <div class="modal fade" id="deleteReward{{ $rewards->id }}" tabindex="-1" aria-labelledby="deleteReward" aria-hidden="true">
              <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header bg-danger">
                  <h1 class="modal-title fs-5" id="exampleModalLabel">Delete Sales Reward</h1>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    <form method="post" action="/master/reward/delete/{{ $rewards->id }}">
                      <h6>Apakah Anda yakin ingin menghapus data Rewards {{ $rewards->id }} ?</h6>
                      <div class="modal-footer">
                        <button type="button" class="btn rounded-pill btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger rounded-pill">Delete</button>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
          </div>
            </td>
          </tr>
        </tbody>
        @empty
            <p>Tidak ditemukan data...</p>
        @endforelse
      </table>

      <div class="container-fluid mb-2 mt-2 d-inline-flex p-2">
        Showing 1 to {{ $data->count() }} of {{ $data->count() }} entries

        <nav aria-label="...">
          <ul class="pagination" style="position: absolute; left: 80%;">
            <li class="page-item disabled">
              <a class="page-link">Previous</a>
            </li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item" aria-current="page">
              <a class="page-link" href="#">2</a>
            </li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
              <a class="page-link" href="#">Next</a>
            </li>
          </ul>
        </nav>
      </div>

    <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>
@endsection