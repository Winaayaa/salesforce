@extends('app')
@section('visit', 'link-primary')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <nav aria-label="breadcrumb" style="--bs-breadcrumb-divider: '>';">
          <h2>Visit Management</h2>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" style="text-decoration: none; color: black">Visit Management</a></li>
            <li class="breadcrumb-item active" aria-current="page" style="color:rgb(139, 178, 225)">Requests</li>
          </ol>
        </nav>
        <div class="btn-toolbar mb-2 mb-md-0">
            {{-- <button type="button" class="btn btn-md btn-success"><strong>+ Tambah</strong></button> --}}
            <button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#addRequests" style="background-color:rgb(139, 178, 225)">
              <strong>+ Add Schedules</strong>
          </button>
          
          <!-- Modal Add Schedules -->
          <div class="modal fade" id="addRequests" tabindex="-1" aria-labelledby="addSales" aria-hidden="true">
              <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header" style="background-color:rgb(139, 178, 225)">
                  <h1 class="modal-title fs-5" id="exampleModalLabel">Add Schedules</h1>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    <form method="post" action="/schedule/requests/insert">
                      <div class="mb-3">
                        <label for="id" class="form-label">ID Schedule</label>
                        <input type="text" class="form-control" id="id" name="id">
                      </div>
                      <div class="mb-3">
                        <label for="name" class="form-label">Event Name</label>
                        <input type="text" class="form-control" id="name" name="name">
                      </div>
                      <div class="mb-3">
                        <label for="date" class="form-label">Date</label>
                        <input type="date" class="form-control" id="date" name="date">
                      </div>
                      <div class="mb-3">
                        <label for="start" class="form-label">Start Time</label>
                        <input type="time" class="form-control" id="start" name="start">
                      </div>
                      <div class="mb-3">
                        <label for="end" class="form-label">End Time</label>
                        <input type="time" class="form-control" id="end" name="end">
                      </div>
                      <div class="mb-3">
                        <label for="location" class="form-label">Location</label>
                        <input type="text" class="form-control" id="location" name="location">
                      </div>
                      <div class="mb-3">
                        <label for="leads" class="form-label">Assigned To</label>
                        <input type="text" class="form-control" id="leads" name="leads">
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn rounded-pill btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary rounded-pill">Save</button>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {{-- End Modal --}}
        </div>
        </div>

      <ul class="nav nav-tabs">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" style="color: rgb(139, 178, 225)" href="/schedule" 
          role="button" aria-expanded="false" aria-controls="leads"><strong>Schedules</strong></a>
        </li>
        <li class="nav-item">
            <a class="nav-link link-light" style="background-color: rgb(139, 178, 225)" aria-current="page" href="/schedule/requests" 
            role="button" aria-expanded="false" aria-controls="customer"><strong>Requests</strong></a>
        </li>
      </ul>

      <div class="container-fluid mb-2 mt-2">
        <span>Show</span>
        <select class="form-select d-inline-flex p-2" aria-label="Default select example" style="width: 70px">
          <option selected>{{ $data->count() }}</option>
          <option value="1">5</option>
          <option value="2">10</option>
          <option value="3">15</option>
        </select>
        entries
        <form class="form d-inline-flex" action="/schedule/requests/find" method="get"
        style="width: 250px; position: absolute; left: 80%">
          <input type="text" name="search" class="form-control" placeholder="Search..." value="{{ request('search') }}">
          {{-- <input type="submit" value="Search"> --}}
          <button class="btn btn-sm" type="submit" style="background-color:rgb(139, 178, 225)">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
              <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
            </svg>
          </button>
        </form>  
      </div>

      <table class="table">
        <thead style="background-color:rgb(139, 178, 225)">
          <tr>
            <th scope="col">No</th>
            <th scope="col">ID</th>
            <th scope="col">Personel</th>
            <th scope="col">Event</th>
            <th scope="col">Date</th>
            <th scope="col">Time</th>
            <th scope="col">Location</th>
            <th scope="col">Status</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        
        @forelse ($data as $requests)
        <tbody>
          <tr>
            <th scope="row">{{ $requests->id }}</th>
            <td>SC000{{ $requests->id }}</td>
            <td>{{ $requests->leads }}</td>
            <td>{{ $requests->name }}</td>
            <td>{{ $requests->date }}</td>
            <td>{{ $requests->start }} sampai {{ $requests->end }}</td>
            <td>{{ $requests->location }}</td>
            <td>Aktif</td>
            <td>
              Modal Edit dan Delete
            </td>
          </tr>
        </tbody>
        @empty
            
        @endforelse
      </table>

    <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>
@endsection