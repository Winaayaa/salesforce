@extends('app')
@section('visit', 'link-primary')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <nav aria-label="breadcrumb" style="--bs-breadcrumb-divider: '>';">
          <h2>Visit Management</h2>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" style="text-decoration: none; color: black">Visit Management</a></li>
            <li class="breadcrumb-item active" aria-current="page" style="color:rgb(139, 178, 225)">Schedules</li>
          </ol>
        </nav>
        <div class="btn-toolbar mb-2 mb-md-0">
            {{-- <button type="button" class="btn btn-md btn-success"><strong>+ Tambah</strong></button> --}}
            <button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#addSchedules" style="background-color:rgb(139, 178, 225)">
                <strong>+ Add Schedules</strong>
            </button>
            
            <!-- Modal Add Schedules -->
            <div class="modal fade" id="addSchedules" tabindex="-1" aria-labelledby="addSales" aria-hidden="true">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Add Schedules</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      <form method="post" action="/schedule/insert">
                        <div class="mb-3">
                          <label for="name" class="form-label">Event Name</label>
                          <input type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="mb-3">
                          <label for="date" class="form-label">Date</label>
                          <input type="date" class="form-control" id="date" name="date">
                        </div>
                        <div class="mb-3">
                          <label for="start" class="form-label">Start Time</label>
                          <input type="time" class="form-control" id="start" name="start">
                        </div>
                        <div class="mb-3">
                          <label for="end" class="form-label">End Time</label>
                          <input type="time" class="form-control" id="end" name="end">
                        </div>
                        <div class="mb-3">
                          <label for="location" class="form-label">Location</label>
                          <input type="text" class="form-control" id="location" name="location">
                        </div>
                        <div class="mb-3">
                          <label for="leads" class="form-label">Assigned To</label>
                          <input type="text" class="form-control" id="leads" name="leads">
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn rounded-pill btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                          <button type="submit" class="btn btn-primary rounded-pill">Save</button>
                          </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              {{-- End Modal --}}
        </div>
        </div>

    <ul class="nav nav-tabs">
        <li class="nav-item">
          <a class="nav-link link-light" style="background-color: rgb(139, 178, 225)" aria-current="page" href="/schedule" 
          role="button" aria-expanded="false" aria-controls="leads"><strong>Schedules</strong></a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" style="color: rgb(139, 178, 225)" aria-current="page" href="/schedule/requests" 
            role="button" aria-expanded="false" aria-controls="customer"><strong>Requests</strong></a>
        </li>
      </ul>

        <div class="container text-center">
          <div class="row">
            <div class="col">
              <div class="card" style="width: 18rem;">
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">Sales Team (2)</h5>
                  <p class="card-text">
                    <ul>
                      <li>Mutia Permatasari</li>
                      <li>Syahrul Ramadhana</li>
                    </ul>
                  </p>
                </div>
              </div>
            </div>
            <div class="col">
              <div class="card" style="width: 18rem;">
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">Calendar</h5>
                  <p class="card-text">May 2023</p>
                  <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
              </div>
            </div>
          </div>
        </div>

    <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>
@endsection