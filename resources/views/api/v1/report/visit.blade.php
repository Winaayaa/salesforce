@extends('app')
@section('report', 'link-primary')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Report</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            {{-- <button type="button" class="btn btn-md btn-success"><strong>+ Tambah</strong></button> --}}
            </div>
            </div>

        <ul class="nav nav-tabs">
            <li class="nav-item">
            <a class="nav-link link-light" style="background-color: rgb(139, 178, 225)" aria-current="page" href="/report" 
            role="button" aria-expanded="false" aria-controls="leads"><strong>Visit</strong></a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" style="color:rgb(139, 178, 225)" aria-current="page" href="/report/sales" 
                role="button" aria-expanded="false" aria-controls="customer"><strong>Sales</strong></a>
            </li>
        </ul>

        <table class="table">
            <thead style="background-color:rgb(139, 178, 225)">
            <tr>
                <th scope="col">No</th>
                <th scope="col">ID</th>
                <th scope="col">Sales Name</th>
                <th scope="col">Total Visit</th>
                <th scope="col">Prospecting</th>
                <th scope="col">Negotiation</th>
                <th scope="col">Won</th>
                <th scope="col">Sign Agreement</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            
            <tbody>
            <tr>
                <th scope="row">Report Sales</th>
                <td>Report</td>
                <td>Report Sales</td>
                <td>Report</td>
                <td>Report Sales</td>
                <td>Report</td>
                <td>Report Sales</td>
                <td>Report</td>
                <td>Report Sales</td>
            </tr>
            </tbody>
        </table>

    <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>
@endsection