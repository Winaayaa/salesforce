<?php

namespace App\Http\Controllers;

// use DB;
use Illuminate\Support\Facades\DB;
use Exception;
use Validator;
use Carbon\Carbon;

use Illuminate\Support\Str;
use Illuminate\Http\Request;

use App\Models\Table\Leads;
use App\Models\Table\LeadsStages;
use App\Models\Table\LeadsActivity;
use App\Models\Table\Schedule;
use App\Models\Table\MasterSales;
use App\Models\Table\MasterReward;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $input = $request->all();

        $id_user = array_key_exists('id_user', $input) ? $input['id_user'] : null;
        $company = array_key_exists('company', $input) ? $input['company'] : null;

        // return view('api.v1.dashboard.index');
        try {
            $id = Leads::where('id_user', $id_user)
                ->where('company', $company)
                ->pluck('id');

            $leads = $this->getLeads($input);
            $activity = $this->getActivity($input);
            $stages = $this->getStages($id);
            $achievement = $this->getActualLeads($id, $company, $id_user);
            $prospect = $this->getProspects($id);

            // return response(view('api/v1/dashboard/index'));
            return response()->json([
                'status' => true,
                'data' => [
                    'leads' => $leads,
                    'activity' => $activity,
                    'stages' => $stages,
                    'achievement' => $achievement,
                    'prospect' => $prospect
                ]
                ]);

                // view('api.v1.dashboard.index');
        } catch(Exception $e) {
            $message = $e->getMessage();

            return response()->json([
                'status' => false,
                'message' => $message
            ]);
        }
    }

    public function indexWeb(Request $request)
    {
        $input = $request->all();

        $company = array_key_exists('company', $input) ? $input['company'] : null;

        try {
            $id = Leads::where('category', 1)
                ->where('company', $company)
                ->pluck('id');

            $closed = $this->getClosedBusiness($id);
            $stages = $this->getStages($id);
            $prospect = $this->getProspects($id);
            $leaderboard = $this->getLeaderboards($id);
    
            return response()->json([
                'status' => true,
                'data' => [
                    'closed' => $closed,
                    'stages' => $stages,
                    'prospect' => $prospect,
                    'leaderboard' => $leaderboard
                ]
            ]);
        } catch(Exception $e) {
            $message = $this->debug ? $e->getMessage() : null;

            return response()->json([
                'status' => false,
                'message' => $message
            ]);
        }
    }

    function getClosedBusiness($id)
    {
        try {
            $actual = LeadsStages::select([
                    DB::raw('sum(cast(amount as bigint)) as total')
                ])
                ->leftJoin('dat_leads', 'dat_leads_stages.id_leads', 'dat_leads.id')
                ->where('activity', 6)
                ->whereIn('id_leads', $id)
                ->groupBy('company_name')
                ->orderBy('total')
                ->first();
    
            return $actual ? $actual->total : 0;
        } catch(Exception $e) {
            return null;
        }
    }

    function getLeads($input)
    {
        $id_user = array_key_exists('id_user', $input) ? $input['id_user'] : null;
        $company = array_key_exists('company', $input) ? $input['company'] : null;

        try {
            $leads = Leads::where('category', 1)
                ->where('id_user', $id_user)
                ->where('company', $company)
                ->count();
                
            $won = Leads::where('category', 1)
                ->where('id_user', $id_user)
                ->where('company', $company)
                ->where('type', '>=', 5)
                ->count();

            $lost = Leads::where('category', 1)
                ->where('id_user', $id_user)
                ->where('company', $company)
                ->where('type', 0)
                ->count();

            return [
                'leads' => $leads,
                'won' => $won,
                'lost' => $lost
            ];
        } catch(Exception $e) {
            return null;
        }
    }

    function getActivity($input) 
    {
        $user = array_key_exists('id_user', $input) ? $input['id_user'] : null;
        $company = array_key_exists('company', $input) ? $input['company'] : null;

        try {
            $visit = Schedule::where('type', 1)
                ->where('id_user', $user)
                ->where('company', $company)
                ->whereNotNull('check_in')
                ->count();

            $online = Schedule::where('type', 2)
                ->where('id_user', $user)
                ->where('company', $company)
                ->whereNotNull('check_in')
                ->count();

            $leads = Leads::where('id_user', $user)
                ->where('company', $company)
                ->pluck('id');

            $email = LeadsActivity::whereIn('id_leads', $leads)
                ->where('activity', 'like', '%Email%')
                ->orWhere('activity', 'like', '%email%')
                ->count();

            $call = LeadsActivity::whereIn('id_leads', $leads)
                ->where('activity', 'like', '%Call%')
                ->orWhere('activity', 'like', '%call%')
                ->count();

            $chat = LeadsActivity::whereIn('id_leads', $leads)
                ->where('activity', 'like', '%Chat%')
                ->orWhere('activity', 'like', '%chat%')
                ->count();

            return [
                'visit' => $visit,
                'online' => $online,
                'email' => $email,
                'call' => $call,
                'chat' => $chat
            ];
        } catch(Exception $e) {
            return null;
        }
    }

    function getActualLeads($id, $company, $user)
    {
        try {
            $goals = MasterReward::select([
                    'target'
                ])
                ->where('company', $company)
                ->where('user', '@>', $user)
                ->whereDate('start_date', '<=', Carbon::now())
                ->whereDate('due_date', '>=', Carbon::now())
                ->first();

            $actual = LeadsStages::select([
                    DB::raw('sum(cast(amount as bigint)) as total')
                ])
                ->leftJoin('dat_leads', 'dat_leads_stages.id_leads', 'dat_leads.id')
                ->where('activity', 6)
                ->whereIn('id_leads', $id)
                ->groupBy('company_name')
                ->orderBy('total')
                ->first();
    
            return [
                'goals' => $goals ? $goals->target : 0,
                'actual' => $actual ? $actual->total : 0
            ];
        } catch(Exception $e) {
            return null;
        }
    }

    function getStages($id)
    {
        try {
            $data = LeadsStages::select([
                    'activity',
                    DB::raw('max(amount) as total')
                ])
                ->where('activity', '!=', '-')
                ->whereIn('id_leads', $id)
                ->groupBy('id_leads')
                ->groupBy('activity')
                ->orderBy('id_leads')
                ->orderBy('activity')
                ->get();

            $res  = [];

            foreach($data as $vals){
                if(array_key_exists($vals['activity'],$res)){
                    $res[$vals['activity']]['total'] += $vals['total'];
                    $res[$vals['activity']]['activity'] = $vals['activity'];
                }
                else{
                    $res[$vals['activity']]  = $vals;
                }
            }
    
            return array_values($res);
        } catch(Exception $e) {
            return null;
        }
    }

    function getProspects($id)
    {
        try {
            $data = LeadsStages::select([
                    'company_name',
                    DB::raw('sum(cast(amount as bigint)) as total')
                ])
                ->leftJoin('dat_leads', 'dat_leads_stages.id_leads', 'dat_leads.id')
                ->where('activity', 1)
                ->whereIn('id_leads', $id)
                ->groupBy('company_name')
                ->orderBy('total', 'desc')
                ->get(5);
    
            return $data;
        } catch(Exception $e) {
            return null;
        }
    }

    function getLeaderboards($id)
    {
        try {
            $data = LeadsStages::select([
                    'name',
                    DB::raw('sum(cast(amount as bigint)) as total')
                ])
                ->leftJoin('dat_leads', 'dat_leads_stages.id_leads', 'dat_leads.id')
                ->where('activity', 6)
                ->whereIn('id_leads', $id)
                ->groupBy('name')
                ->orderBy('total', 'desc')
                ->get();
    
            return $data;
        } catch(Exception $e) {
            return null;
        }
    }
}
