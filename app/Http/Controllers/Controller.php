<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected $debug;

    public function __construct() {
        $this->debug = (bool) env('APP_DEBUG', true);
    }
}
