<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SalesReward;
use App\SalesReward as AppSalesReward;

class RewardController extends Controller
{
    //
    public function store(Request $request) {
        // $reward = SalesReward::create([
        //     'id' => '0001',
        //     'name' => 'Rewards 1',
        //     'from' => '2021-10-01',
        //     'to' => '2021-10-31',
        //     'value' => '20',
        //     'parameter' => '1.000.000',
        //     'type' => 'Amount', 
        //     'gift' => 'Kulkas 3 Pintu',           
        //     'target' => '500.000',
        // ]);
        // dump($reward);

        $reward = new SalesReward();

        $reward->id = $request->id;
        $reward->name = $request->name;
        $reward->from = $request->from;
        $reward->to = $request->to;
        $reward->value = $request->value;
        $reward->parameter = $request->parameter;
        $reward->type = $request->type;
        $reward->gift = $request->gift;
        $reward->target = $request->target;

        $reward->save();

        return redirect('/master/reward');
    }

    public function show() 
    {
        $data = SalesReward::get();

        return view('api.v1.master.reward', 
        ['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        $update = SalesReward::find($id);

        $update->name = $request->name;
        $update->from = $request->from;
        $update->to = $request->to;
        $update->value = $request->value;
        $update->parameter = $request->parameter;
        $update->type = $request->type;
        $update->gift = $request->gift;
        $update->target = $request->target;

        $update->save();
        return redirect('/master/reward');
    }

    public function delete($id)
    {
        SalesReward::destroy($id);

        return redirect('/master/reward');
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $data = SalesReward::where('name', 'like', "%".$search."%")->paginate();

        return view('api.v1.master.reward', ['data' => $data]);
    }
}