<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SalesTeam;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

class SalesController extends Controller
{
    public function store(Request $request)
    {
        // $sales = SalesTeam::create([
        //     'id' => '0001',
        //     'fullname' => 'Mutia Permatasari',
        //     'branch' => 'Cabang Bandung',
        //     'position' => 'Human Resource',
        //     'report' => 'Jimmy Yogaswara',
        //     'level' => 'Permanent',
        //     'targetdeals' => '1.500.000',
        //     'targetvisit' => '5',

        // ]);
        // dump($sales);

        $sales = new SalesTeam;

        $sales->id = $request->id;
        $sales->fullname = $request->fullname;
        $sales->branch = $request->branch;
        $sales->level = $request->level;
        $sales->position = $request->position;
        $sales->report = $request->report;
        $sales->targetdeals = $request->targetdeals;
        $sales->targetvisit = $request->targetvisit;

        $sales->save();

        return redirect('/master/sales');
    }

    public function show() 
    {
        $data = SalesTeam::get();

        return view('api.v1.master.sales', 
        ['data' => $data]);
    }

    public function update(Request $request, $id) 
    {
        $update = SalesTeam::find($id);

        $update->fullname = $request->fullname;
        $update->branch = $request->branch;
        $update->level = $request->level;
        $update->position = $request->position;
        $update->report = $request->report;
        $update->targetdeals = $request->targetdeals;
        $update->targetvisit = $request->targetvisit;

        $update->save();
        return redirect('/master/sales');
    }

    public function delete($id)
    {
        SalesTeam::destroy($id);

        return redirect('/master/sales');
    }

    public function search(Request $request)
    {
        $search = $request->search;

        $data = SalesTeam::where('fullname', 'like', "%".$search."%")->paginate();
        return view('api.v1.master.sales', ['data' => $data]);
        // return response()->json($data);
    }
}