<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Leads;

class LeadsController extends Controller
{
    public function store(Request $request)
    {
        // $leads = Leads::create([
        //     'id' => '0001',
        //     'fullname' => 'Mutia Permatasari',
        //     'phone' => '(704) 555-0127',
        //     'email' => 'permatasarimutia1@gmail.com',
        //     'company' => 'Louis Vuitton',
        //     'com_phone' => '(480) 555-0103',
        //     'com_address' => 'Lorem ipsum dolor sit amet',
        //     'type' => 'Demo or Meeting',
        //     'channel' => 'Whatsapp',
        //     'customer' => 'Theresa Webb',
        // ]);
        // dump($leads);

        $leads = new Leads;

        $leads->fullname = $request->fullname;
        $leads->phone = $request->phone;
        $leads->email = $request->email;
        $leads->company = $request->company;
        $leads->com_phone = $request->com_phone;
        $leads->com_address = $request->com_address;
        $leads->type = $request->type;
        $leads->channel = $request->channel;
        $leads->customer = $request->customer;

        $leads->save();
        return redirect('/leads');
    }

    public function show()
    {
        $data = Leads::get();

        return view('api.v1.leads.leads', 
        ['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        $update = Leads::find($id);

        $update->fullname = $request->fullname;
        $update->phone = $request->phone;
        $update->email = $request->email;
        $update->company = $request->company;
        $update->com_phone = $request->com_phone;
        $update->com_address = $request->com_address;
        $update->type = $request->type;
        $update->channel = $request->channel;
        $update->customer = $request->customer;

        $update->save();
        return redirect('/leads');
    }

    public function delete($id)
    {
        Leads::destroy($id);

        return redirect('/leads');
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $data = Leads::where('fullname', 'like', "%".$search."%")->paginate();
        return view('api.v1.leads.leads', ['data' => $data]);
    }

    public function filter(Request $request)
    {
        $type = $request->type;
        $assign_to = $request->assign_to;

        $data = Leads::where('type', 'like', "%".$type."%", 'and', 
        'customer', 'like', "%".$assign_to."%")->paginate();
        return view('api.v1.leads.leads', ['data' => $data]);
    }
}