<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;

class CustomerController extends Controller
{
    public function store(Request $request)
    {
        // $customer = Customer::create([
        //     'id' => '0001',
        //     'fullname' => 'Mutia Permatasari',
        //     'phone' => '(704) 555-0127',
        //     'company' => 'Louis Vuitton',
        //     'com_phone' => '(480) 555-0103',
        //     'type' => 'Demo or Meeting', 
        //     'assign_to' => 'Theresa Webb',
        // ]);
        // dump($customer);
        
        $customer = new Customer;

        $customer->id = $request->id;
        $customer->fullname = $request->fullname;
        $customer->phone = $request->phone;
        $customer->company = $request->company;
        $customer->com_phone = $request->com_phone;
        $customer->type = $request->type;
        $customer->assign_to = $request->assign_to;

        $customer->save();
        return redirect('/leads/customer');
    }

    public function show()
    {
        $data = Customer::get();

        return view('api.v1.leads.customer',
        ['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        $update = Customer::find($id);

        $update->fullname = $request->fullname;
        $update->phone = $request->phone;
        $update->company = $request->company;
        $update->com_phone = $request->com_phone;
        $update->type = $request->type;
        $update->assign_to = $request->assign_to;

        $update->save();
        return redirect('/leads/customer');
    }

    public function delete($id)
    {
        Customer::destroy($id);

        return redirect('/leads/customer');
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $data = Customer::where('fullname', 'like', "%".$search."%")->paginate();
        return view('api.v1.leads.customer', ['data' => $data]);
    }

    public function filter(Request $request)
    {
        $type = $request->type;
        $assign_to = $request->assign_to;

        $data = Customer::where('type', 'like', "%".$type."%", 'and', 
        'assign_to', 'like', "%".$assign_to."%")->paginate();
        return view('api.v1.leads.customer', ['data' => $data]);
    }
}