<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Schedule;

class RequestsController extends Controller
{
    //
    public function store(Request $request)
    {
        $schedule = new Schedule;
        
        $schedule->id = $request->id;
        $schedule->name = $request->name;
        $schedule->date = $request->date;
        $schedule->start = $request->start;
        $schedule->end = $request->end;
        $schedule->location = $request->location;
        $schedule->leads = $request->leads;

        $schedule->save();
        return redirect('/schedule/requests');
    }

    public function show()
    {
        $data = Schedule::get();

        return view('api.v1.schedule.requests', 
        ['data' => $data]);
    }

    public function update(Request $request, $id) 
    {
        $update = Schedule::find($id);

        $update->name = $request->name;
        $update->date = $request->date;
        $update->start = $request->start;
        $update->end = $request->end;
        $update->location = $request->location;
        $update->leads = $request->leads;

        return redirect('/schedule/requests');
    }

    public function delete($id)
    {
        Schedule::destroy($id);

        return redirect('/schedule/requests');
    }

    public function search(Request $request) 
    {
        $search = $request->search;
        $data = Schedule::where('name', 'like', "%".$search."%")->paginate();
        return view('api.v1.schedule.requests', ['data' => $data]);
    }

}