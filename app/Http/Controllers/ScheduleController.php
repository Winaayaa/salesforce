<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Schedule;

class ScheduleController extends Controller
{
    //
    public function store(Request $request)
    {
        $schedule = new Schedule;
        
        $schedule->id = $request->id;
        $schedule->name = $request->name;
        $schedule->date = $request->date;
        $schedule->start = $request->start;
        $schedule->end = $request->end;
        $schedule->location = $request->location;
        $schedule->leads = $request->leads;

        $schedule->save();
        return redirect('/schedule');
    }

    public function show()
    {
        $data = Schedule::get();

        return view('api.v1.schedule.schedules', 
        ['data' => $data]);
    }

    public function update(Request $request, $id) 
    {
        $update = Schedule::find($id);

        $update->name = $request->name;
        $update->date = $request->date;
        $update->start = $request->start;
        $update->end = $request->end;
        $update->location = $request->location;
        $update->leads = $request->leads;

        return redirect('/schedule');
    }

    public function delete($id)
    {
        Schedule::destroy($id);

        return redirect('/schedule');
    }
}