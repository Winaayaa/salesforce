<?php

namespace App\Exports\Master;

use App\Models\Table\MasterSales;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MasterSalesExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return MasterSales::get()->map(function ($voter) {
            return [
                "id_user" => $voter->id_user
            ];
        });
    }

    public function headings(): array
    {
        return [
            'User'
        ];
    }
}
