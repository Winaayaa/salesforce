<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SalesTeam extends Model
{
    //
    protected $table = 'salesteams';

    use HasFactory;
    protected $guarded = [];
}