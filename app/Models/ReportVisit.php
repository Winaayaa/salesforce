<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ReportVisit extends Model
{
    protected $table = 'reportvisits';
    use HasFactory;
    protected $guarded = [];
    
}