<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SalesReward extends Model
{
    protected $table = 'salesrewards';
    use HasFactory;
    protected $guarded = [];
}